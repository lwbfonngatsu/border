import torch
import torch.nn as nn
import torch.nn.functional as F


class ConvBnReLU(nn.Module):
    def __init__(self, in_channels, out_channels, batchnorm=True, activation=True, **kwargs):
        super(ConvBnReLU, self).__init__()
        # kernel_size, stride, padding
        self.conv = nn.Conv2d(in_channels, out_channels, bias=False, **kwargs)
        self.norm = nn.BatchNorm2d(out_channels)
        self.batchnorm = batchnorm
        self.activation = activation

    def forward(self, x):
        if self.batchnorm:
            out = self.norm(self.conv(x))
        else:
            out = self.conv(x)
        if self.activation:
            out = F.relu(out, inplace=True)
        return out


class Pooling(nn.Module):
    def __init__(self, pooling_type, **kwargs):
        super(Pooling, self).__init__()
        # kernel_size, stride, padding, dilation, return_indices, ceil_mode
        if 'max' in pooling_type:
            self.pooling = nn.MaxPool2d(**kwargs)
        elif 'avg' in pooling_type:
            self.pooling = nn.AvgPool2d(**kwargs)
    
    def forward(self, x):
        return self.pooling(x)

    
class Network(nn.Module):
    def __init__(self, config):
        super(Network, self).__init__()
        self.features = nn.Sequential()
        stage = {
            'conv': 0,
            'pooling': 0
        }
        
        for i, layer in enumerate(config):
            if 'conv' in layer:
                bn = True if 'bn' in layer else False
                act = True if 'relu' in layer else False
                name = 'conv%s%s%s' % (('_bn' if bn else ''), ('_relu' if act else ''), ("_%d" % stage['conv']))
                self.features.add_module(name, 
                                         ConvBnReLU(
                                             config[layer]['in_chn'], config[layer]['out_chn'], bn, act, 
                                             kernel_size=config[layer]['kernel_size'], 
                                             stride=config[layer]['stride'], 
                                             padding=config[layer]['padding']
                                         )
                                        )
                stage['conv'] += 1
            if 'pool' in layer:
                assert 'max' in layer or 'avg' in layer, 'Pooling type only support [\'max\', \'avg\']'
                ptype = 'max' if 'max' in layer else 'avg'
                name = 'pool%s%s' % (('_max' if 'max' in layer else '_avg'), ('_%d' % stage['pooling']))
                self.features.add_module(name, 
                                         Pooling(
                                             pooling_type=ptype,
                                             kernel_size=config[layer]['kernel_size'], 
                                             stride=config[layer]['stride'],
                                             padding=config[layer]['padding']
                                         )
                                        )
                
    def forward(self, x):
        for feat in self.features:
            output = feat(x)
        return output