import csv
from thop import profile
from torchsummary import summary


class MLIndicator(object):
    def __init__(self, filename, op, chn_range=(1, 32), kns_range=(1, 5), stride_range=(1, 5), padding_range=(1, 5)):
        self.start = True
        
        self.op = op
        self.name = self.op
        self.channel_range = chn_range
        self.kernel_range = kns_range
        self.stride_range = stride_range
        self.padding_range = padding_range
        
        self.out_chn = self.channel_range[0]
        self.kernel_size = self.kernel_range[0]
        self.stride = self.stride_range[0]
        self.padding = self.padding_range[0]
        
        total_op = 3 if self.op == 'conv' else 1
        total_chn = self.channel_range[1] - self.channel_range[0] + 1
        total_ken = self.kernel_range[1] - self.kernel_range[0] + 1
        total_str = self.stride_range[1] - self.stride_range[0] + 1
        total_pad = self.padding_range[1] - self.padding_range[0] + 1
        self.total_run = total_op * total_chn * total_ken * total_str * total_pad
        
        self.file = open(filename, "w")
        self.writer = csv.writer(self.file)
        self.writer.writerow([''] * 12)
        self.writer.writerow([f'Operator: {self.op}'])
        self.writer.writerow([f'Output channel range: {self.channel_range}'])
        self.writer.writerow([f'Kernel size range: {self.kernel_range}'])
        self.writer.writerow([f'Stride range: {self.stride_range}'])
        self.writer.writerow([f'Padding range: {self.padding_range}'])
    
    def generate_config(self):
        config = dict()
        if not self.start:
            if self.padding < self.padding_range[1]:
                self.padding += 1
            else:
                self.padding = self.padding_range[0]
                self.stride += 1

            if self.stride > self.stride_range[1]:
                self.stride = self.stride_range[0]
                self.kernel_size += 1

            if self.kernel_size > self.kernel_range[1]:
                self.kernel_size = self.kernel_range[0]
                self.out_chn += 1

            if self.out_chn > self.channel_range[1]:
                self.out_chn = self.channel_range[0]
                if self.op is 'conv':
                    if 'bn' not in self.name:
                        self.name += '_bn'
                    elif 'relu' not in self.name:
                        self.name += '_relu'
        else:
            self.start = False
        config[self.name] = {
            'in_chn': 3,
            'out_chn': self.out_chn,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            'padding': self.padding
        }
        return config
    
    def run(self):
        self.writer.writerow('')
        self.writer.writerow('')
        self.writer.writerow('')
        self.writer.writerow(['Layer Type', 'Output Channel', 'Kernel Size', 'Stride', 'Padding', 'Params', 'MACs', 'FLOPs', 'Avg. Process Time (ms)', 'Avg. Temp (C)', 'Avg. Power (W)'])
        print(self.total_run)
        for _ in range(self.total_run):
            config = self.generate_config()
            network = Network(config)
            
            dummy_inp = torch.randn(1, 3, 304, 304)
            macs, params = profile(network, inputs=(dummy_inp, ))
            flops = 2 * macs
            _ = summary(network, verbose=False)
            # print(f"MAC: {macs/(10**9):.2f} G ({macs:.0f})")
            # print(f"FLOPs: {flops/(10**9):.2f} G ({flops:.0f})")
            # print()
            self.writer.writerow([self.name, self.out_chn, self.kernel_size, self.stride, self.padding, params, macs, flops])
        self.file.close()

indicator = MLIndicator('log.csv', op='conv')
indicator.run()