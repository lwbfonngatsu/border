import sys
sys.path.append("../../libs")

import os
import cv2
import warnings
warnings.filterwarnings('ignore')

import time
import torch
import shutil
import argparse
import torch.utils.data as data
import torch.backends.cudnn as cudnn

from networks.Pelee.layers.functions import PriorBox
from networks.Pelee.data import detection_collate
from networks.Pelee.configs.CC import Config
from networks.Pelee.utils.core import *

parser = argparse.ArgumentParser(description='Pelee Training')
parser.add_argument('-n', '--name', default='miracle_gauge')
parser.add_argument('-c', '--config', default='/home/protomate/Desktop/border/archives/weights/PeleeNet/configs/miracle_gauge.py')
parser.add_argument('--ngpu', default=1, type=int, help='gpus')
parser.add_argument('--resume_net', default=None, help='resume net for retraining')
parser.add_argument('--resume_epoch', default=0, type=int, help='resume iter for retraining')
parser.add_argument('-t', '--tensorboard', action='store_true', help='Use tensorborad to show the Loss Graph')
parser.add_argument('-m', '--mixup', action='store_true', help="Enable dataset mixup")
args = parser.parse_args()

if 'reduced' in args.config:
    if 'v2' in args.config:
        from networks.Pelee.peleenet_reduced_v2 import build_net
    else:
        from networks.Pelee.peleenet_reduced import build_net
elif 'stride2' in args.config:
    from networks.Pelee.peleenet_stride2 import build_net
elif 'miracle' in args.config:
    from networks.Pelee.peleenet_miracle import build_net
else:
    from networks.Pelee.peleenet import build_net


print_info('----------------------------------------------------------------------\n'
           '|                       Pelee Training Program                       |\n'
           '----------------------------------------------------------------------', ['yellow', 'bold'])

logger = set_logger(args.tensorboard, path="/home/protomate/Desktop/border/archives")
global cfg
cfg = Config.fromfile(args.config)
net = build_net('train', cfg.model.input_size, cfg.model)
init_net(net, cfg, args.resume_net)

if args.ngpu > 1:
    net = torch.nn.DataParallel(net)
if cfg.train_cfg.cuda:
    net.cuda()
    cudnn.benckmark = True

optimizer = set_optimizer(net, cfg)
criterion = set_criterion(cfg)
priorbox = PriorBox(anchors(cfg.model))

with torch.no_grad():
    priors = priorbox.forward()
    if cfg.train_cfg.cuda:
        priors = priors.cuda()


if __name__ == '__main__':
    parameters = f"Config path: {args.config}\nInput shape: {cfg.model.input_size} x {cfg.model.input_size}\nClass number: {cfg.model.num_classes}\nPrior num: {priors.shape}"
    print_info(parameters, ['yellow', 'bold'])
    
    net.train()
    epoch = args.resume_epoch
    print_info('===> Loading Dataset...', ['yellow', 'bold'])
    dataset = get_dataloader(cfg, 'train', args.mixup)


    epoch_size = len(dataset) // (cfg.train_cfg.per_batch_size * args.ngpu)
    max_iter = cfg.train_cfg.step_lr[-1] + 1

    stepvalues = cfg.train_cfg.step_lr

    start_iter = args.resume_epoch * epoch_size if args.resume_epoch > 0 else 0
    step_index = 0
    for step in stepvalues:
        if start_iter > step:
            step_index += 1
    
    for iteration in range(start_iter, max_iter):
        if iteration % epoch_size == 0:
            batch_iterator = iter(data.DataLoader(dataset,
                                                  cfg.train_cfg.per_batch_size * args.ngpu,
                                                  shuffle=True,
                                                  num_workers=cfg.train_cfg.num_workers,
                                                  collate_fn=detection_collate
                                                 ))
            if epoch % cfg.model.save_epochs == 0:
                save_checkpoint(net, cfg, final=False, name=args.name, epoch=epoch)
            epoch += 1
        load_t0 = time.time()
        if iteration in stepvalues:
            step_index += 1
        lr = adjust_learning_rate(optimizer, step_index, cfg)
        images, targets = next(batch_iterator)
        if cfg.train_cfg.cuda:
            images = images.cuda()
            targets = [anno.cuda() for anno in targets]
        out = net(images)
        
        optimizer.zero_grad()
        loss_l, loss_c = criterion(out, priors, targets)
        loss = loss_l + loss_c
        
        _images = None
        
#         @TODO detected images logger
#         if iteration % cfg.train_cfg.print_epochs == 0:
#             _images = []
#             for img in ori_images:
#                 w, h = img.shape[1], img.shape[0]
#                 scale = torch.Tensor([w, h, w, h])
#                 loc = out[0].view(1, -1, 4)
#                 softmax = torch.nn.Softmax(dim=-1)
#                 conf = softmax(out[1].view(-1, numclasses))
#                 out = (loc, conf)
#                 boxes, scores = detector.forward(out, priors)
#                 boxes = (boxes[0] * scale).cpu().numpy()
#                 scores = scores[0].cpu().numpy()
                
#                 cv2.rectangle(img, )
                
        
        write_logger({'loc_loss': loss_l.item(),
                      'conf_loss': loss_c.item(),
                      'loss': loss.item()},
                     _images,
                     logger,
                     iteration,
                     status=args.tensorboard)
        loss.backward()
        optimizer.step()
        load_t1 = time.time()
        print_train_log(iteration, cfg.train_cfg.print_epochs, [time.ctime(), epoch, iteration % epoch_size, epoch_size, iteration, loss_l.item(), loss_c.item(), load_t1 - load_t0, lr])

    save_checkpoint(net, cfg, final=True, name=args.name, epoch=-1)
