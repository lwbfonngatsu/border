import sys
sys.path.append("../../libs")
sys.path.append("../../libs/networks/yolor")

import os
import cv2
import time
import torch
import argparse

from pathlib import Path
from evaluate import evaluate
from utils.datasets import LoadImages
from models.experimental import attempt_load
from utils.torch_utils import select_device, time_synchronized
from utils.general import check_img_size, non_max_suppression, scale_coords, xyxy2xywh, increment_path


def EvalYOLOR(imgsz=640, conf_thres=0.5):
    root = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/Datasets/PTMDataset"
    source = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/Datasets/PTMDataset/test.txt"
    weights = "../../archives/weights/YOLOR/yolor-d6.pt"
    save_txt = True
    project = "../../archives/eval_results/yolor"
    name = "yolor_d6"
    conf_thres = 0.5
    iou_thres = 0.45
    classes = [1, 2, 3, 5, 7] # None for every classes

    save_dir = Path(increment_path(Path(project) / name, exist_ok=False))
    (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)
    device = select_device('0')
    model = attempt_load(weights, map_location=device)
    imgsz = check_img_size(imgsz, s=model.stride.max())
    dataset = LoadImages(source, img_size=imgsz, auto_size=64, root=root, ext='jpg')
    names = model.module.names if hasattr(model, 'module') else model.names

    gt = os.path.join(root, "PTMFormat")
    pd = os.path.join(save_dir, "labels")

    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)
    _ = model(img) if device.type != 'cpu' else None
    for path, img, im0s, _ in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.float()
        img /= 255.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        t1 = time_synchronized()
        pred = model(img, augment=False)[0]
        pred = non_max_suppression(pred, conf_thres, iou_thres, classes=classes, agnostic=False)
        t2 = time_synchronized()

        for i, det in enumerate(pred):
            p, s, im0 = Path(path), '', im0s
            save_path = str(save_dir / p.name)
            txt_path = str(save_dir / 'labels' / p.stem) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
            s += '%gx%g ' % img.shape[2:]
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]
            if len(det):
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()
                det[:, -1] = 1
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()
                    s += '%g %ss, ' % (n, names[int(c)])

                for *xyxy, conf, cls in reversed(det):
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4))).view(-1).tolist()
                        xywh = [int(a) for a in xywh]
                        xywh[0] -= xywh[2]//2
                        xywh[1] -= xywh[3]//2
                        line = (1, conf, *xywh)
                        with open(txt_path + '.txt', 'a') as f:
                            f.write(('%g ' * len(line)).rstrip() % line + '\n')
    if save_txt:
        print('Results saved to %s' % save_dir)
    print('Done. (%.3fs)' % (time.time() - t0))

    evaluate.evaluate(gt, pd, conf_thres)
    
if __name__ == "__main__":
    EvalYOLOR(imgsz=640, conf_thres=0.5)