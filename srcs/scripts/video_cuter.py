from re import I
import cv2
import os
import shutil


ROOT = '/mnt/3694a998-c714-43da-99b1-66f85757d25d/gauge_video'
BATCHFORMAT = 'G1S'
SCENE = 1
FOURCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')

start_schedules = { # (minute, second)
    0: [(2, 30), (4, 10), (5, 30), (7, 5), (7, 20)],
    1: [(2, 10), (3, 0), (4, 40), (6, 50)],
    2: [(0, 0), (1, 50), (3, 40), (5, 40), (8, 0)],
    3: [(0, 20), (1, 50)],
    4: [],
    5: [],
    6: [(4, 20), (7, 10), (8, 0)],
    7: [(0, 0)],
    8: [(8, 30)],
    9: [(0, 0), (4, 15)],
}
stop_schedules = { # (minute, second)
    0: [(3, 10), (5, 10), (6, 0), (7, 15), (7, 30)],
    1: [(2, 30), (3, 20), (5, 0), (8, 10)],
    2: [(0, 10), (2, 10), (4, 20), (6, 30), (8, 20)],
    3: [(1, 0), (2, 10)],
    4: [],
    5: [],
    6: [(5, 0), (7, 30), (8, 10)],
    7: [(1, 40)],
    8: [(9, 50)],
    9: [(1, 10), (5, 0)]
}

def get_frame_pos(t, fps):
    minute = t[0]
    second = t[1]
    return (minute * 60 + second) * fps

video_names = [f'20220726_{i}.mp4' for i in [102907, 120741, 120826, 120843,120915,121018 ]]
idx = -1
os.makedirs('videos', exist_ok=True)
for video_name in video_names:
    video_path = os.path.join(ROOT, video_name)
    if os.path.exists(video_path):
        idx += 1
        video = cv2.VideoCapture(video_path)
        start_list = start_schedules[idx]
        stop_list = stop_schedules[idx]
        fps = int(video.get(cv2.CAP_PROP_FPS))
        w, h = int(video.get(cv2.CAP_PROP_FRAME_WIDTH)), int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        if len(start_list) == 0 and len(stop_list) == 0:
            continue

        for start, stop in zip(start_list, stop_list):
            scene = BATCHFORMAT + str(SCENE)
            print(scene, start, stop)

            writer = cv2.VideoWriter(f"videos/{scene}.mp4", FOURCC, fps, (w, h))
            video.set(cv2.CAP_PROP_POS_FRAMES, get_frame_pos(start, fps) - 1)
            while video.get(cv2.CAP_PROP_POS_FRAMES) <= get_frame_pos(stop, fps):
                ret, frame = video.read()
                if ret:
                    writer.write(frame)
                else:
                    break
            writer.release()
            SCENE += 1

    else:
        print(f'Path not found: {video_path}, Skip')
