import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import os
import cv2
import sys
import torch
import argparse
import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import torch.backends.cudnn as cudnn

sys.path.append("/home/protomate/Desktop/border/libs")

from nms import nms
from tqdm import tqdm
from torch.autograd import Variable
from networks.Pelee.data import BaseTransform
from networks.Pelee.configs.CC import Config
from networks.Pelee.utils.core import anchors, init_net
from networks.Pelee.layers.functions import Detect, PriorBox


ARCHIVESROOT = "/home/protomate/Desktop/border/archives"
WEIGHTROOT = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/gauge/"
DATAROOT = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/gauge/Dataset"
SUPPORTING_PRUNE_TYPE = ['l1_unstructured']

def voc_ap(rec, prec, use_07_metric=False):
    if use_07_metric:
        ap = 0.
        for t in np.arange(0., 1.1, 0.1):
            if np.sum(rec >= t) == 0:
                p = 0
            else:
                p = np.max(prec[rec >= t])
            ap = ap + p / 11.
    else:
        mrec = np.concatenate(([0.], rec, [1.]))
        mpre = np.concatenate(([0.], prec, [0.]))

        for i in range(mpre.size - 1, 0, -1):
            mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

        i = np.where(mrec[1:] != mrec[:-1])[0]
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def prune_l1_unstructured(model, layer_type, proportion):
    for module in model.modules():
        if isinstance(module, layer_type):
            prune.l1_unstructured(module, 'weight', proportion)
            prune.remove(module, 'weight')
    return model


class PeleeNet(object):
    def __init__(self, cfg_path, weight_path, thr):
        cfg = Config.fromfile(cfg_path)
        anchor_config = anchors(cfg.model)
        priorbox = PriorBox(anchor_config)
        self.net = build_net('test', cfg.model.input_size, cfg.model)
        init_net(self.net, cfg, weight_path)
        self.net.eval()
        self.num_classes = cfg.model.num_classes
        self.thresh = thr
        self.nms_threshold = 0.3
        self.softmax = torch.nn.Softmax(dim=-1)
        
        with torch.no_grad():
            self.priors = priorbox.forward()
            if cfg.test_cfg.cuda:
                self.net = self.net.cuda()
                self.priors = self.priors.cuda()
                cudnn.benchmark = True
            else:
                self.net = self.net.cpu()

        self.detector = Detect(self.num_classes, cfg.loss.bkg_label, anchor_config)
        self.t = BaseTransform(cfg.model.input_size, cfg.model.rgb_means, (2, 0, 1))            
    
    def prune(self, type='l1_unstructured', op=nn.Conv2d, amount=0.8):
        assert amount <= 0.9, "Pruning amount must equal or below 0.9"
        if type == 'l1_unstructured':
            self.net = prune_l1_unstructured(self.net, op, amount)
    
    def predict(self, path, verbose=False):
        with torch.no_grad():
            folder_ = path.split("/")[-3]
            if folder_ in ["1", "2", "3"]:
                image = cv2.imread(path, 1)
            else:    
                image = cv2.imread(path, 1|cv2.IMREAD_IGNORE_ORIENTATION)
            img = Variable(self.t(image).unsqueeze(0)).cuda()
            w, h = image.shape[1], image.shape[0]

            out = self.net(img)
            loc = out[0].view(1, -1, 4)

            conf = self.softmax(out[1].view(-1, self.num_classes))
            out = (loc, conf)
            scale = torch.Tensor([w, h, w, h]).cuda()
            boxes, scores = self.detector.forward(out, self.priors)

            boxes = (boxes[0] * scale).cpu().numpy()
            scores = scores[0].cpu().numpy()
            all_boxes = [[] for _ in range(self.num_classes)]

            for j in range(1, self.num_classes):
                inds = np.where(scores[:, j] > self.thresh)[0]
                if len(inds) == 0:
                    all_boxes[j] = np.empty([0, 5], dtype=np.int)
                    continue
                c_bboxes = boxes[inds]
                c_scores = scores[inds, j]

                c_dets = np.hstack((c_bboxes, c_scores[:, np.newaxis])).astype(np.int, copy=False)
                keep = nms(c_dets, self.nms_threshold)
                keep = keep[:50]
                c_dets = c_dets[keep, :]
                all_boxes[j] = c_dets.astype(np.int)
            
            if verbose:
                print("Prediction")
                for i, box in enumerate(all_boxes[1]):
                    x, y, x2, y2 = [0 if point <= 0 else point for point in box[:4].astype(np.int).tolist()]
                    print(f" {i}", (x, y, x2, y2))
                    
            for label in range(len(all_boxes)):
                if np.any(all_boxes[label]):
                    for box in all_boxes[label]:
                        x, y, x2, y2 = [0 if point <= 0 else point for point in box[:4].astype(np.int).tolist()]
                        image = cv2.rectangle(image, (x, y), (x2, y2), (0, 255, 0), 5)
            return all_boxes[1:], image
        

def confusion(predicts, groundtruths):
    nd = 0
    loss = []
    file = open(test_text, "r")
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]
    for boxes in predicts:
        for box in boxes[0]:
            nd += 1
    tp, fp, fn = np.zeros(nd), np.zeros(nd), 0
    assert len(predicts) == len(groundtruths), "Number of predicted images and groundtruth images doesn't match"
    d = 0
    
    for i in range(len(predicts)):
        pd = predicts[i][0]
        gt = groundtruths[i][0]
        for label in gt:
            if len(pd) > 0:
                ious = np.zeros(len(pd))
                for j, pred in enumerate(pd):
                    ixmin = np.maximum(label[0], pred[0])
                    iymin = np.maximum(label[1], pred[1])
                    ixmax = np.minimum(label[2], pred[2])
                    iymax = np.minimum(label[3], pred[3])
                    iw = np.maximum(ixmax - ixmin + 1., 0.)
                    ih = np.maximum(iymax - iymin + 1., 0.)
                    inters = iw * ih
                    uni = (
                        (pred[2] - pred[0] + 1.) * (pred[3] - pred[1] + 1.) + 
                        (label[2] - label[0] + 1.) * (label[3] - label[1] + 1.) - 
                        inters
                    )
                    iou = inters / uni
                    ious[j] = iou
                match = np.argmax(ious)
                if ious[match] > 0.5:
                    tp[d] = 1.
                    d += 1
                    pd = np.delete(pd, match, axis=0)
                else:
                    fn += 1
                    loss.append(lines[i])
            else:
                fn += 1
                loss.append(lines[i])

                
        for k in range(len(pd)):
            fp[d] = 1.
            d += 1
    
    print(loss)
    _tp = np.cumsum(np.array(tp))
    _fp = np.cumsum(np.array(fp))
    rec = _tp / float(1112)
    prec = _tp / np.maximum(_tp + _fp, np.finfo(np.float64).eps)
    ap = voc_ap(rec, prec)
    
    return int(_tp[-1]), int(_fp[-1]), fn, prec[-1], rec[-1], ap


def voc_ap(rec, prec, use_07_metric=False):
    if use_07_metric:
        ap = 0.
        for t in np.arange(0., 1.1, 0.1):
            if np.sum(rec >= t) == 0:
                p = 0
            else:
                p = np.max(prec[rec >= t])
            ap = ap + p / 11.
    else:
        mrec = np.concatenate(([0.], rec, [1.]))
        mpre = np.concatenate(([0.], prec, [0.]))

        for i in range(mpre.size - 1, 0, -1):
            mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

        i = np.where(mrec[1:] != mrec[:-1])[0]
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Pelee Evaluation (PTM Version)')
    parser.add_argument('-c', '--config', default='/home/protomate/Desktop/border/archives/weights/PeleeNet/configs/color_gauge.py', type=str)
    parser.add_argument('-m', '--trained_model', default='color_gauge/Pelee_color_gauge_size304_epoch200.pth', type=str, help='Trained state_dict file path to open')
    parser.add_argument('-p', '--prune', action='store_true')
    parser.add_argument('-t', '--prune_type', type=str)
    parser.add_argument('-o', '--op_type', type=str)
    parser.add_argument('-a', '--amount', type=float)
    args = parser.parse_args()
    
    if args.prune:
        if args.prune_type is None and args.op_type is None and args.amount is None:
            raise "Error, prune (-p) requires prune_type (-t) and op_type (-o)"
        else:
            assert args.prune_type.lower() in SUPPORTING_PRUNE_TYPE, f"Error, prune type must be in {SUPPORTING_PRUNE_TYPE}."
            assert args.amount > 0.0 and args.amount < 1.0, "Pruning amount must be in range of [0.0, 1.0]."

    from networks.Pelee.peleenet import build_net
#     from networks.Pelee.peleenet_miracle import build_net
        
    
    cfg_name = args.config
    folder = args.trained_model.split('/')[0]
    name = args.trained_model.split('/')[1]
    thr = 0.5
    save = False
    if save:
        os.makedirs(os.path.join(ARCHIVESROOT, "outputs", folder), exist_ok=True)

    cfg_path = os.path.join(WEIGHTROOT, "configs", cfg_name)
    weight_path = os.path.join(WEIGHTROOT, folder, name)
    # weight_path = name
    pelee = PeleeNet(cfg_path, weight_path, thr)
    test_text = os.path.join(DATAROOT, "test.txt")

    if args.prune:
        if args.op_type.lower() == 'conv2d':
            pruning_layer = nn.Conv2d
        pelee.prune(args.prune_type, pruning_layer, args.amount)
    
    all_pd = []
    all_gt = []
    npd = 0
    ngt = 0
    
    with open(test_text, "r") as file:
        lines = file.readlines()
        for name in tqdm(lines):
            name = name.strip()
            image_path = os.path.join(DATAROOT, name.split("_")[0], "JPEGImages", name + ".jpg")
            label_path = os.path.join(DATAROOT, name.split("_")[0], "Annotations", name + ".xml")
            tree = ET.parse(label_path)
            target = tree.getroot()
            gt_boxes = [[] for _ in range(2)]
            pd_boxes, output_image = pelee.predict(image_path, verbose=False)
#             print(pd_boxes)
            count = sum(1 for _ in target.iter("object"))
            if count == 0:
                gt_boxes[0] = np.empty([0, 5], dtype=np.int)
                gt_boxes[1] = np.empty([0, 5], dtype=np.int)
            else:
                gt = []
                for i, obj in enumerate(target.iter('object')):
                    x = int(obj.find('bndbox/xmin').text)
                    y = int(obj.find('bndbox/ymin').text)
                    x2 = int(obj.find('bndbox/xmax').text)
                    y2 = int(obj.find('bndbox/ymax').text)
                    gt.append([x, y, x2, y2])
                    output_image = cv2.rectangle(output_image, (x, y), (x2, y2), (0, 127, 255), 5)
                    ngt += 1
                gt_boxes[1] = np.array(gt, dtype=np.int)
                
            if save:
                cv2.imwrite(os.path.join(ARCHIVESROOT, f"outputs/{folder}/{name}.jpg"), output_image)
            
            npd += pd_boxes[0].shape[0]
            all_pd.append(pd_boxes)
            all_gt.append(gt_boxes[1:])
            
    tp, fp, fn, prec, rec, ap = confusion(all_pd, all_gt)
    print(f"\nModel = {folder}")
    print(f"Confident Threshold = {thr}")
    
    print(f"True Positive = {tp}")
    print(f"False Positive = {fp}")
    print(f"False Negative = {fn}")
    print(f"Precision = {prec:.3f}")
    print(f"Recall = {rec:.3f}")
    print(f"Accuracy = {tp / (tp + fp + fn):.3f}")
    print(f"mAP = {ap:.3f}")
    