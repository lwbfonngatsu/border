import matplotlib.pyplot as plt
# %matplotlib inline 
import PIL.Image as pilimg
import numpy as np
import torch
import torch.utils.data as data
import xml.etree.ElementTree as ET
import numpy as np
import os
import cv2
import sys

sys.path.append("../../../libs")
from config import opt
from networks.ResNet.augmentations import preproc_for_test, preproc_for_train

VOC_LABELS = ["gauge"]



class VOCDetection(data.Dataset):


    def __init__(self, opt, is_train=True):
        self.is_train = is_train
        self.opt = opt   
        self.ids = []
        
        root_path = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/gauge/Dataset"
        train_script = os.path.join(root_path, 'train.txt')
        test_script = os.path.join(root_path, 'test.txt')
        if is_train:
            with open(train_script, 'r') as f:
                for line in f.readlines():
                    line = line.strip()
                    folder = line.split("_")[0]

                    ano_path = os.path.join(root_path, folder, 'Annotations', line + '.xml')
                    img_path = os.path.join(root_path, folder, 'JPEGImages', line + '.jpg')
                    # print(ano_path)
                    self.ids.append((img_path, ano_path,folder))
        else:
            with open(test_script, 'r') as f:
                for line in f.readlines():
                    line = line.strip()
                    folder = line.split("_")[0]

                    ano_path = os.path.join(root_path, folder, 'Annotations', line + '.xml')
                    img_path = os.path.join(root_path, folder, 'JPEGImages', line + '.jpg')
                    # print(ano_path)
                    self.ids.append((img_path, ano_path,folder))



    
    
    def __getitem__(self, index):
        img_path, ano_path, folder = self.ids[index]
        if folder in ["1", "2", "3"]:
            image = cv2.imread(img_path)
        else:    
            image = cv2.imread(img_path, cv2.IMREAD_COLOR|cv2.IMREAD_IGNORE_ORIENTATION)
        boxes, labels = self.get_annotations(ano_path)
        
        if self.is_train:
            image, boxes, labels = preproc_for_train(image, boxes, labels, opt.min_size, opt.mean)
            image = torch.from_numpy(image)
           
        
        
        target = np.concatenate([boxes, labels.reshape(-1,1)], axis=1)
        
        return image, target



    def get_annotations(self, path):
        
        tree = ET.parse(path)

        boxes = []
        labels = []
        
        for child in tree.getroot():
            if child.tag != 'object':
                continue

            bndbox = child.find('bndbox')
            box =[
                float(bndbox.find(t).text) - 1
                for t in ['xmin', 'ymin', 'xmax', 'ymax']
            ]


            # label = VOC_LABELS.index(child.find('name').text) 
            
            boxes.append(box)
            labels.append(0)


        return np.array(boxes), np.array(labels)

    
    def __len__(self):
        return len(self.ids)
    
    
if __name__ == '__main__':  
    
    
    a=VOCDetection(opt, is_train=True)
#     a[0]

    print(len(a))
    # print(a.ids[0][0])