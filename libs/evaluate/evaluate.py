#----------------test detmet module-------------------------------

from detmet import DetMetric
from detutils import *
from BoundingBox import BoundingBox
from BoundingBoxes import BoundingBoxes

import numpy as np
import glob
import os

def getBoundingBoxes(folderGT, folderDet):
    files = os.listdir(folderGT)
    files.sort()

    allBoundingBoxes = BoundingBoxes()
    # Read GT detections from txt file
    # Each line of the files in the groundtruths folder represents a ground truth bounding box
    # (bounding boxes that a detector should detect)

    # Each value of each line is  "class_id, x, y, width, height" respectively
    # Class_id represents the class of the bounding box
    # x, y represents the most top-left coordinates of the bounding box
    # x2, y2 represents the most bottom-right coordinates of the bounding box
    all_gt = 0
    for f in files:   # file in GT-folder
        if ".txt" in f:
            nameOfImage = f.replace(".txt", "")
            fh1 = open(os.path.join(folderGT, f), "r")
            for line in fh1:
                line = line.replace("\n", "")
                if line.replace(' ', '') == '':
                    continue
                splitLine = [a.strip() for a in line.split(",")]

                idClass = splitLine[0]  # class
                x = float(splitLine[1])  # GT no confidnce
                y = float(splitLine[2])
                w = float(splitLine[3])
                h = float(splitLine[4])

                bb = BoundingBox(
                    nameOfImage,
                    idClass,
                    x,
                    y,
                    w,
                    h,
                    CoordinatesType.Absolute, None,
                    BBType.GroundTruth,
                    format=BBFormat.XYWH)
                allBoundingBoxes.addBoundingBox(bb)
                all_gt += 1
            fh1.close()

    # ----------------------------Read detections------------------------------
    files = os.listdir(folderDet)
    # files = glob.glob("./detections/*.txt")
    files.sort()
    # Read detections from txt file
    # Each line of the files in the detections folder represents a detected bounding box.

    # Each value of each line is  "class_id, confidence, x, y, width, height" respectively
    # Class_id represents the class of the detected bounding box
    # Confidence represents confidence (from 0 to 1) that this detection belongs to the class_id.
    # x, y represents the most top-left coordinates of the bounding box
    # x2, y2 represents the most bottom-right coordinates of the bounding box
    all_pd = 0
    for f in files:
        # nameOfImage = f.replace("_det.txt","")
        if ".txt" in f:
            nameOfImage = f.replace(".txt", "")
            # Read detections from txt file
            fh1 = open(os.path.join(folderDet,f), "r")
            for line in fh1:
                line = line.replace("\n", "")
                if line.replace(' ', '') == '':
                    continue
                splitLine = line.split(" ")
                idClass = splitLine[0]  # class
                confidence = float(splitLine[1])  # confidence predict detect
                x = float(splitLine[2])
                y = float(splitLine[3])
                w = float(splitLine[4])
                h = float(splitLine[5])
                bb = BoundingBox(
                    nameOfImage,
                    idClass,
                    x,
                    y,
                    w,
                    h,
                    CoordinatesType.Absolute, None,
                    BBType.Detected,  # GT or predict
                    confidence,
                    format=BBFormat.XYWH)
                allBoundingBoxes.addBoundingBox(bb)
                all_pd += 1
            fh1.close()
    return allBoundingBoxes, all_gt, all_pd


def createImages(dictGroundTruth, dictDetected):
    """
    Create representative images with bounding boxes.
    """
    import numpy as np
    import cv2
    # Define image size
    width = 200
    height = 200
    # Loop through the dictionary with ground truth detections
    for key in dictGroundTruth:
        image = np.zeros((height, width, 3), np.uint8)
        gt_boundingboxes = dictGroundTruth[key]
        image = gt_boundingboxes.drawAllBoundingBoxes(image)
        detection_boundingboxes = dictDetected[key]
        image = detection_boundingboxes.drawAllBoundingBoxes(image)
        # Show detection and its GT
        cv2.imshow(key, image)
        cv2.waitKey()
        

def evaluate(gt, pd, thresh):
    # Read txt files containing bounding boxes (ground truth and detections)
    root = ""
    boundingboxes, all_gt, all_pd = getBoundingBoxes(gt, pd)
    # Uncomment the line below to generate images based on the bounding boxes
#     createImages(dictGroundTruth, dictDetected)
    # Create an evaluator object in order to obtain the metrics
    evaluator = DetMetric()

    ##############################################################
    # VOC PASCAL Metrics
    ##############################################################
    # Plot Precision x Recall curve
    evaluator.PlotPRCurve(
        boundingboxes,  # Object containing all bounding boxes (ground truths and detections)
        IOUThreshold=thresh,  # IOU threshold
        # MethodAveragePrecision.COCOInterpolation: COCO AP, MethodAveragePrecision.ElevenInterpolationElevenPointInterpolation: VOC2008,
        # MethodAveragePrecision.EveryPointInterpolation: VOC2010
        method=MethodAveragePrecision.COCOInterpolation,
        showAP=True,  # Show Average Precision in the title of the plot
        savePath=os.path.join(root, "P-R-Curve-VOC.png"),
        showInterpolatedPrecision=False)  # Plot the interpolated precision curve


    # Get metrics with PASCAL VOC metrics
    metricsPerClass, metricsAll = evaluator.GetDetMetrics(
        boundingboxes,  # Object containing all bounding boxes (ground truths and detections)
        IOUThreshold=thresh,  # IOU threshold
        beta = 1,  # F1-score
        method=MethodAveragePrecision.ElevenPointInterpolation)

    print("Average precision values per class:\n")
    # Loop through classes to obtain their metrics

    s = ('%20s' + '%14s' * 7) % ('Class', 'P', 'R', 'F-Score', 'total TP', 'total FP', 'total FN', 'VOC AP@.5')
    print(s)
    for mc in metricsPerClass:
        # Get metric values per each class
        c = mc['class']
        precision = mc['precision']
        recall = mc['recall']
        f_score = mc['f_score']
        average_precision = mc['AP']
        tp = mc['total TP']
        fp = mc['total FP']
        fn = all_gt - tp - fp
        metric_c = ('%20s' + '%14s' * 7) % (c, round(precision[-1], 3), round(recall[-1], 3), round(f_score, 3),  tp, fp, fn, round(average_precision, 3))

        # Print AP per class
        print(metric_c)

    all_precision = round(metricsAll["all_precision"], 3)
    all_recall = round(metricsAll["all_recall"], 3)
    all_f_score = round(metricsAll["all_f_score"], 3)
    all_ap = round(metricsAll["total_map"], 3)

    metric_all = ('%20s' + '%14s' * 6) % ("all", all_precision, all_recall, all_f_score,  "\\", "\\", "mAP@50:{}".format(all_ap))

    # Print voc mAP for all class
    print(metric_all)

    #########################################################################
    # COCO Metrics
    # Precision, Recall, F-Score are same to VOC, so we only compute COCO AP
    #########################################################################
    # Plot Precision x Recall curve
    evaluator.PlotPRCurve(
        boundingboxes,  # Object containing all bounding boxes (ground truths and detections)
        IOUThreshold=0.5,  # IOU threshold
        # MethodAveragePrecision.COCOInterpolation: COCO AP, MethodAveragePrecision.COCOInterpolationElevenPointInterpolation: VOC2008,
        # MethodAveragePrecision.EveryPointInterpolation: VOC2010
        method=MethodAveragePrecision.ElevenPointInterpolation, # we do not compute the COCO AP for every class, so we use VOC AP replace,in P-R Curve!
        showAP=True,  # Show Average Precision in the title of the plot
        savePath=os.path.join(root, "P-R-Curv-COCO.png"),
        showInterpolatedPrecision=False)  # Plot the interpolated precision curve

    # Get metrics with COCO metrics
    coco_ap_dict = {}
    for viou in np.linspace(0.5, 0.95, num=10):
        metricsPerClass, metricsAll = evaluator.GetDetMetrics(
            boundingboxes,      # Object containing all bounding boxes (ground truths and detections)
            IOUThreshold=viou,  # IOU threshold
            beta = 1,           # F1-score
            method=MethodAveragePrecision.COCOInterpolation)

        ap_iou = metricsAll["total_map"]

        coco_ap_dict[str(viou)] = ap_iou
        print(" Average Precision  (AP)  AP@{}: {}".format(int(viou*100), round(ap_iou,3)))

    COCO_AP = np.mean(np.array(list(coco_ap_dict.values())))
    print(" Average Precision  (AP)  AP@50:95: {}".format(COCO_AP))
    
    
def big_picture(gt_path, pd_path):
    import glob
    all_gt_path = glob.glob(os.path.join(gt_path, "*.txt"))
    all_pd_path = glob.glob(os.path.join(pd_path, "*.txt"))
    first_image = all_gt_path[0]