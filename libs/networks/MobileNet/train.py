import os
import argparse
import logging
import sys
import itertools
from tqdm import tqdm

import torch
from torch.utils.data import DataLoader, ConcatDataset
from torch.optim.lr_scheduler import CosineAnnealingLR, MultiStepLR
from torch.utils.tensorboard import SummaryWriter

from utils.misc import str2bool, Timer, freeze_net_layers, store_labels
from ssd.ssd import MatchPrior
from ssd.config import mobilenetv1_ssd_config
from ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite
from datasets.open_images import OpenImagesDataset
from nn.multibox_loss import MultiboxLoss
from ssd.data_preprocessing import TrainAugmentation, TestTransform

opt = mobilenetv1_ssd_config.opt

parser = argparse.ArgumentParser(
    description='Single Shot MultiBox Detector Training With Pytorch')
parser.add_argument("--config_path", default="./ssd/config/mobilenetv1_ssd_config.py", type=str,
                    help='Specify config path. default = ./ssd/config/mobilenetv1_ssd_config.py')

# Params for loading pretrained basenet or checkpoints.
parser.add_argument('--base_net',
                    help='Pretrained base model')
parser.add_argument('--pretrained_ssd', help='Pre-trained base model')
parser.add_argument('--resume', default=None, type=str,
                    help='Checkpoint state_dict file to resume training from')


logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
args = parser.parse_args()
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print(DEVICE)

if opt.use_cuda and torch.cuda.is_available():
    torch.backends.cudnn.benchmark = True
    logging.info("Use Cuda.")

def train(loader, net, criterion, optimizer, device, debug_steps=100, epoch=-1):
    net.train(True)
    running_loss = 0.0
    running_regression_loss = 0.0
    running_classification_loss = 0.0

    # for tensorboard
    writer = SummaryWriter()
    n_iter = 0

    for i, data in tqdm(enumerate(loader)):
        images, boxes, labels = data
        images = images.to(device)
        boxes = boxes.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()
        confidence, locations = net(images)
        regression_loss, classification_loss = criterion(confidence, locations, labels, boxes)  # TODO CHANGE BOXES
        loss = regression_loss + classification_loss
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        running_regression_loss += regression_loss.item()
        running_classification_loss += classification_loss.item()

        # logging into tensorboard
        writer.add_scalar('Train/Loss', loss.item(), n_iter)
        writer.add_scalar('Train/Regression Loss', regression_loss.item(), n_iter)
        writer.add_scalar('Train/Classification Loss', classification_loss.item(), n_iter)
        n_iter += 1

        if i and i % debug_steps == 0:
            avg_loss = running_loss / debug_steps
            avg_reg_loss = running_regression_loss / debug_steps
            avg_clf_loss = running_classification_loss / debug_steps
            logging.info(
                f"Epoch: {epoch}, Step: {i}, " +
                f"Average Loss: {avg_loss:.4f}, " +
                f"Average Regression Loss {avg_reg_loss:.4f}, " +
                f"Average Classification Loss: {avg_clf_loss:.4f}"
            )
            running_loss = 0.0
            running_regression_loss = 0.0
            running_classification_loss = 0.0


def test(loader, net, criterion, device):
    net.eval()
    running_loss = 0.0
    running_regression_loss = 0.0
    running_classification_loss = 0.0
    num = 0
    for _, data in enumerate(loader):
        images, boxes, labels = data
        images = images.to(device)
        boxes = boxes.to(device)
        labels = labels.to(device)
        num += 1

        with torch.no_grad():
            confidence, locations = net(images)
            regression_loss, classification_loss = criterion(confidence, locations, labels, boxes)
            loss = regression_loss + classification_loss

        running_loss += loss.item()
        running_regression_loss += regression_loss.item()
        running_classification_loss += classification_loss.item()
    return running_loss / num, running_regression_loss / num, running_classification_loss / num


if __name__ == '__main__':
    timer = Timer()

    if opt.net == 'mb2-ssd-lite':
        create_net = lambda num: create_mobilenetv2_ssd_lite(num, width_mult=opt.mb2_width_mult)
        config = mobilenetv1_ssd_config
    else:
        logging.fatal("The net type is wrong.")
        parser.print_help(sys.stderr)
        sys.exit(1)
    train_transform = TrainAugmentation(config.image_size, config.image_mean, config.image_std)
    target_transform = MatchPrior(config.priors, config.center_variance,
                                  config.size_variance, 0.5)

    test_transform = TestTransform(config.image_size, config.image_mean, config.image_std)

    logging.info("Prepare training datasets.")
    if opt.dataset_type == 'open_images':
        dataset = OpenImagesDataset(opt,
             transform=train_transform, target_transform=target_transform,
             dataset_type="train", balance_data=opt.balance_data)
        print(dataset)
        label_file = os.path.join(opt.checkpoint_folder, "open-images-model-labels.txt")
        store_labels(label_file, dataset.class_names)
        logging.info(dataset)
        num_classes = len(dataset.class_names)

    else:
        raise ValueError(f"Dataset type {opt.dataset_type} is not supported.")
    logging.info(f"Stored labels into file {label_file}.")
    train_dataset = ConcatDataset([dataset])
    logging.info("Train dataset size: {}".format(len(train_dataset)))
    train_loader = DataLoader(train_dataset, opt.batch_size,
                              num_workers=opt.num_workers,
                              shuffle=True)
    logging.info("Prepare Validation datasets.")
    if opt.dataset_type == 'open_images':
        val_dataset = OpenImagesDataset(opt,
                                        transform=test_transform, target_transform=target_transform,
                                        dataset_type="test")
        logging.info(val_dataset)
    logging.info("validation dataset size: {}".format(len(val_dataset)))

    val_loader = DataLoader(val_dataset, opt.batch_size,
                            num_workers=opt.num_workers,
                            shuffle=False)
    logging.info("Build network.")
    net = create_net(num_classes)
    min_loss = -10000.0
    last_epoch = -1

    base_net_lr = opt.base_net_lr if opt.base_net_lr is not None else opt.lr
    extra_layers_lr = opt.extra_layers_lr if opt.extra_layers_lr is not None else opt.lr
    if opt.freeze_base_net:
        logging.info("Freeze base net.")
        freeze_net_layers(net.base_net)
        params = itertools.chain(net.source_layer_add_ons.parameters(), net.extras.parameters(),
                                 net.regression_headers.parameters(), net.classification_headers.parameters())
        params = [
            {'params': itertools.chain(
                net.source_layer_add_ons.parameters(),
                net.extras.parameters()
            ), 'lr': extra_layers_lr},
            {'params': itertools.chain(
                net.regression_headers.parameters(),
                net.classification_headers.parameters()
            )}
        ]
    elif opt.freeze_net:
        freeze_net_layers(net.base_net)
        freeze_net_layers(net.source_layer_add_ons)
        freeze_net_layers(net.extras)
        params = itertools.chain(net.regression_headers.parameters(), net.classification_headers.parameters())
        logging.info("Freeze all the layers except prediction heads.")
    else:
        params = [
            {'params': net.base_net.parameters(), 'lr': base_net_lr},
            {'params': itertools.chain(
                net.source_layer_add_ons.parameters(),
                net.extras.parameters()
            ), 'lr': extra_layers_lr},
            {'params': itertools.chain(
                net.regression_headers.parameters(),
                net.classification_headers.parameters()
            )}
        ]

    timer.start("Load Model")
    if args.resume:
        logging.info(f"Resume from the model {opt.resume}")
        net.load(opt.resume)
    elif args.base_net:
        logging.info(f"Init from base net {opt.base_net}")
        net.init_from_base_net(opt.base_net)
    elif args.pretrained_ssd:
        logging.info(f"Init from pretrained ssd {opt.pretrained_ssd}")
        net.init_from_pretrained_ssd(opt.pretrained_ssd)
    logging.info(f'Took {timer.end("Load Model"):.2f} seconds to load the model.')

    net.to(DEVICE)

    criterion = MultiboxLoss(config.priors, iou_threshold=0.5, neg_pos_ratio=3,
                             center_variance=0.1, size_variance=0.2, device=DEVICE)
    optimizer = torch.optim.SGD(params, lr=opt.lr, momentum=opt.momentum,
                                weight_decay=opt.weight_decay)
    logging.info(f"Learning rate: {opt.lr}, Base net learning rate: {base_net_lr}, "
                 + f"Extra Layers learning rate: {extra_layers_lr}.")

    if opt.scheduler == 'multi-step':
        logging.info("Uses MultiStepLR scheduler.")
        milestones = [int(v.strip()) for v in opt.milestones.split(",")]
        scheduler = MultiStepLR(optimizer, milestones=milestones,
                                                     gamma=0.1, last_epoch=last_epoch)
    elif opt.scheduler == 'cosine':
        logging.info("Uses CosineAnnealingLR scheduler.")
        scheduler = CosineAnnealingLR(optimizer, opt.t_max, last_epoch=last_epoch)
    else:
        logging.fatal(f"Unsupported Scheduler: {opt.scheduler}.")
        parser.print_help(sys.stderr)
        sys.exit(1)

    logging.info(f"Start training from epoch {last_epoch + 1}.")
    for epoch in range(last_epoch + 1, opt.num_epochs):
        scheduler.step()
        train(train_loader, net, criterion, optimizer,
              device=DEVICE, debug_steps=opt.debug_steps, epoch=epoch)
        
        # if epoch % args.validation_epochs == 0 or epoch == args.num_epochs - 1:
        #     val_loss, val_regression_loss, val_classification_loss = test(val_loader, net, criterion, DEVICE)
        #     logging.info(
        #         f"Epoch: {epoch}, " +
        #         f"Validation Loss: {val_loss:.4f}, " +
        #         f"Validation Regression Loss {val_regression_loss:.4f}, " +
        #         f"Validation Classification Loss: {val_classification_loss:.4f}"
        #     )
        model_path = os.path.join(args.checkpoint_folder, f"{args.net}-Epoch-{epoch}.pth")
        net.save(model_path)
        logging.info(f"Saved model {model_path}")