import numpy as np

from utils.box_utils import SSDSpec, SSDBoxSizes, generate_ssd_priors


image_size = 300
image_mean = np.array([127, 127, 127])  # RGB layout
image_std = 128.0
iou_threshold = 0.45
center_variance = 0.1
size_variance = 0.2

specs = [
    SSDSpec(19, 16, SSDBoxSizes(60, 105), [2, 3]),
    SSDSpec(10, 32, SSDBoxSizes(105, 150), [2, 3]),
    SSDSpec(5, 64, SSDBoxSizes(150, 195), [2, 3]),
    SSDSpec(3, 100, SSDBoxSizes(195, 240), [2, 3]),
    SSDSpec(2, 150, SSDBoxSizes(240, 285), [2, 3]),
    SSDSpec(1, 300, SSDBoxSizes(285, 330), [2, 3])
]

class Config:
    
    class_name = ['BACKGROUND', "gauge"]
    dataset_type = "open_images"
    dataset_path = "/mnt/3694a998-c714-43da-99b1-66f85757d25d/gauge/Dataset"
    net = "mb2-ssd-lite"

    freeze_base_net = None

    freeze_net = None

    mb2_width_mult = 1.0

    # Params for SGD
    lr = 1e-3 # initial learning rate
    momentum = 0.9 # Momentum value for optim
    weight_decay = 5e-4 # Weight decay for SGD
    gamma = 0.1 # Gamma update for SGD
    base_net_lr = None # initial learning rate for base net
    extra_layers_lr = None # initial learning rate for the layers not in base net and prediction heads
    balance_data=False
    # Scheduler
    scheduler = "multi-step" # Scheduler for SGD. It can one of multi-step and cosine
    milestones = "80,100" # milestones for MultiStepLR

    t_max = 120 # T_max value for Cosine Annealing Scheduler.
    
    batch_size = 16
    num_epochs = 120
    num_workers = 1
    validation_epochs = 5
    debug_steps = 100
    use_cuda = True
    checkpoint_folder = "model/"

priors = generate_ssd_priors(specs, image_size)

opt = Config()