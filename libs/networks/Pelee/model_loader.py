import os
import cv2
import sys
import numpy as np

from PIL import Image
from utilize.nms import nms
from utilize.utils import *
from torchvision import transforms
from utilize.tracker import Tracker
from prettytable import PrettyTable
from torch.autograd import Variable
from networks.Pelee.utils.core import *
from networks.Pelee.data import BaseTransform
from networks.Pelee.configs.CC import Config
from networks.Pelee.utils.timer import Timer
from networks.Pelee.peleenet import build_net
from networks.Pelee.layers.functions import Detect, PriorBox


class Model(object):
    def __init__(self, config_path, model_path, threshold=0.6, nms_threshold=0.45):
        cfg = Config.fromfile(config_path)
        anchor_config = anchors(cfg.model)
        priorbox = PriorBox(anchor_config)
        self.net = build_net('test', cfg.model.input_size, cfg.model)
        init_net(self.net, cfg, model_path)
        self.net.eval()
        self.num_classes = cfg.model.num_classes
        self.thresh = threshold
        self.nms_threshold = nms_threshold
        
        with torch.no_grad():
            self.priors = priorbox.forward()
            if cfg.test_cfg.cuda:
                self.net = self.net.cuda()
                self.priors = self.priors.cuda()
                cudnn.benchmark = True
            else:
                self.net = self.net.cpu()

        self.detector = Detect(self.num_classes, cfg.loss.bkg_label, anchor_config)
        self.t = BaseTransform(cfg.model.input_size, cfg.model.rgb_means, (2, 0, 1))
        
    def predict(self, image):
        with torch.no_grad():
            img = Variable(self.t(image).unsqueeze(0)).cuda()
            img = img.cuda()
            w, h = image.shape[1], image.shape[0]
            out = net(img)

            scale = torch.Tensor([w, h, w, h])
            boxes, scores = self.detector.forward(out, self.priors)
            boxes = (boxes[0] * scale).cpu().numpy()
            scores = scores[0].cpu().numpy()
            all_boxes = [[] for _ in range(self.num_classes)]

            for j in range(1, self.num_classes):
                inds = np.where(scores[:, j] > self.thresh)[0]
                if len(inds) == 0:
                    all_boxes[j] = np.empty([0, 5], dtype=np.float32)
                    continue
                c_bboxes = boxes[inds]
                c_scores = scores[inds, j]
                c_dets = np.hstack((c_bboxes, c_scores[:, np.newaxis])).astype(np.float32, copy=False)

                keep = nms(c_dets, self.nms_threshold)
                keep = keep[:50]
                c_dets = c_dets[keep, :]
                all_boxes[j] = c_dets
        return all_boxes
    
    def draw_box(image, boxes):
        for label in range(len(boxes)):
            if np.any(boxes[label]):
                for box in boxes[label]:
                    x, y, x2, y2 = [0 if point <= 0 else point for point in box[:4].astype(np.int).tolist()]
                    image = cv2.rectangle(image, (x, y), (x2, y2), (0, 255, 0), 5)
        return image