from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os
import cv2
import sys
import torch
import pickle
import random
import numpy as np
import torch.utils.data as data
import torchvision.transforms as transforms

from .voc_eval import voc_eval
from PIL import Image, ImageDraw, ImageFont

if sys.version_info[0] == 2:
    import xml.etree.cElementTree as ET
else:
    import xml.etree.ElementTree as ET

# VOC_CLASSES = ('__background__',  # always index 0
#                'aeroplane', 'bicycle', 'bird', 'boat',
#                'bottle', 'bus', 'car', 'cat', 'chair',
#                'cow', 'diningtable', 'dog', 'horse',
#                'motorbike', 'person', 'pottedplant',
#                'sheep', 'sofa', 'train', 'tvmonitor')

# VOC_CLASSES = ('__background__', 'vehicle')
VOC_CLASSES = ('__background__', "PI6_9", "PI10_9", "PI6_19", "PI4_19", "PI4_19", "PI4_4", "PI10_4" , "PI6_4", "PI4_9", "PI10_9", "PI6_9", "PI4_19", "PI6_19", "PI16_9")

# VOC_CLASSES = ('__background__', 'bicycle', 'bus', 'car', 'motorbike')
# for making bounding boxes pretty
COLORS = ((255, 0, 0, 128), (0, 255, 0, 128), (0, 0, 255, 128),
          (0, 255, 255, 128), (255, 0, 255, 128), (255, 255, 0, 128))

class AnnotationTransform(object):
    def __init__(self, class_to_ind=None, keep_difficult=True):
        self.class_to_ind = class_to_ind or dict(zip(VOC_CLASSES, range(len(VOC_CLASSES))))
        self.keep_difficult = keep_difficult

    def __call__(self, target):
        res = np.empty((0, 5))
        for obj in target.iter('object'):
            if obj.find('name') is None:
                continue
            name = obj.find('name').text.strip()
            if name in VOC_CLASSES:
                difficult = int(obj.find('difficult').text) == 1
                if not self.keep_difficult and difficult:
                    continue
                bbox = obj.find('bndbox')
                pts = ['xmin', 'ymin', 'xmax', 'ymax']
                bndbox = []
                for i, pt in enumerate(pts):
                    cur_pt = int(bbox.find(pt).text) - 1
                    # scale height or width
                    # cur_pt = cur_pt / width if i % 2 == 0 else cur_pt / height
                    bndbox.append(cur_pt)
                label_idx = self.class_to_ind[name]
                bndbox.append(label_idx)
                # [xmin, ymin, xmax, ymax, label_ind]
                res = np.vstack((res, bndbox))
                # img_id = target.find('filename').text[:-4]
            else:
                continue
        return res  # [[xmin, ymin, xmax, ymax, label_ind], ... ]


class VOCDetection(data.Dataset):
    def __init__(self, root, phase='train', preproc=None, target_transform=AnnotationTransform(), mixup=False, alpha=0.3):
        self.root = root
        self.phase = phase
        self.alpha = alpha
        self.mixup = mixup
        self.preproc = preproc
        self.target_transform = target_transform
        self.lam = np.random.beta(self.alpha, self.alpha)
        self._annopath = os.path.join('%s', 'Annotations', '%s.xml')
        self._imgpath = os.path.join('%s', 'JPEGImages', '%s.jpg')
        self.ids = list()
        # Change this
        for line in open(os.path.join(self.root, f'{phase}.txt')):
            self.ids.append((os.path.join(self.root, line.split("_")[0]), line.strip()))

    def __getitem__(self, index):
        img_id = self.ids[index]
        target = ET.parse(self._annopath % img_id).getroot()
        # img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)
        folder = self._imgpath.split("/")[-3]
        if folder in ["1", "2", "3"]:
            img = cv2.imread(self._imgpath % img_id)
        else:    
            img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR|cv2.IMREAD_IGNORE_ORIENTATION)
        if self.target_transform is not None:
            target = self.target_transform(target)
        if self.preproc is not None:
            img, target = self.preproc(img, target)
        # Mixup every 5 images
        if self.phase == "train" and index > 0 and index % 5 == 0 and self.mixup:
            mixup_idx = random.randint(0, len(self.ids) - 1)
            mixup_img_id = self.ids[index]
            mixup_target = ET.parse(self._annopath % mixup_img_id).getroot()
            if folder in ["1", "2", "3"]:
                mixup_img = cv2.imread(self._imgpath % mixup_img_id, cv2.IMREAD_COLOR)
            else: 
                mixup_img = cv2.imread(self._imgpath % mixup_img_id, cv2.IMREAD_COLOR|cv2.IMREAD_IGNORE_ORIENTATION)
            folder_ = self._imgpath.split("/")[-3]
            if folder in ["1", "2", "3"]:
                img = cv2.imread(self._imgpath % img_id)
            else:    
                img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR|cv2.IMREAD_IGNORE_ORIENTATION)
            
            if self.target_transform is not None:
                mixup_target = self.target_transform(mixup_target)
            if self.preproc is not None:
                mixup_img, mixup_target = self.preproc(mixup_img, mixup_target)
            img = (self.lam * img + (1 - self.lam) * mixup_img)
            target = np.array(target.tolist() + mixup_target.tolist())
#             target = target
        return img, target

    def __len__(self):
        return len(self.ids)

    def pull_image(self, index):
        img_id = self.ids[index]
        # return cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)
        folder = self._imgpath.split("/")[-3]
        if folder in ["1", "2", "3"]:
            return cv2.imread(self._imgpath % img_id )
        else:    
            return cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR|cv2.IMREAD_IGNORE_ORIENTATION)
        

    def pull_anno(self, index):
        img_id = self.ids[index]
        anno = ET.parse(self._annopath % img_id).getroot()
        gt = self.target_transform(anno, 1, 1)
        return img_id[1], gt

    def pull_tensor(self, index):
        to_tensor = transforms.ToTensor()
        return torch.Tensor(self.pull_image(index)).unsqueeze_(0)

    def evaluate_detections(self, all_boxes, output_dir=None, thr=0.5):
        self._write_voc_results_file(all_boxes)
        self._do_python_eval(output_dir, thr)

    def _get_voc_results_file_template(self):
        filename = 'comp4_det_test' + '_{:s}.txt'
        filedir = os.path.join(self.root, 'results', 'VOC2012')
        if not os.path.exists(filedir):
            os.makedirs(filedir)
        path = os.path.join(filedir, filename)
        return path

    def _write_voc_results_file(self, all_boxes):
        for cls_ind, cls in enumerate(VOC_CLASSES):
            cls_ind = cls_ind
            if cls == '__background__':
                continue
            print('Writing {} VOC results file'.format(cls))
            filename = self._get_voc_results_file_template().format(cls)            
            with open(filename, 'wt') as f:
                for im_ind, index in enumerate(self.ids):
                    index = index[1]
                    dets = all_boxes[cls_ind][im_ind]
                    if dets == []:
                        continue
                    for k in range(dets.shape[0]):
                        f.write('{:s} {:.3f} {:.1f} {:.1f} {:.1f} {:.1f}\n'.
                                format(index, dets[k, -1],
                                       dets[k, 0] + 1, dets[k, 1] + 1,
                                       dets[k, 2] + 1, dets[k, 3] + 1))

    def _do_python_eval(self, output_dir='output', thr=0.5):
        rootpath = os.path.join(self.root)
        annopath = os.path.join(rootpath, '{:s}', 'Annotations','{:s}.xml')
        imagesetfile = os.path.join(rootpath, f'{self.phase}.txt')
        cachedir = os.path.join(self.root, 'annotations_cache')
        aps = []
        # The PASCAL VOC metric changed in 2010
        use_07_metric = False
        print('VOC07 metric? ' + ('Yes' if use_07_metric else 'No'))
        if output_dir is not None and not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        for i, cls in enumerate(VOC_CLASSES):
            if cls == '__background__':
                continue
            filename = self._get_voc_results_file_template().format(cls)
            rec, prec, ap, tp, fp, nd, fn = voc_eval(filename, annopath, imagesetfile, cls, cachedir, ovthresh=thr, use_07_metric=use_07_metric)
            aps += [ap]
            print(f'Class {cls}')
            print(f'AP = {ap}')
            print(f'TP = {tp[-1]}')
            print(f'FP = {fp[-1]}')
            pre = prec[-1]
            re = prec[-1]
            
            if output_dir is not None:
                with open(os.path.join(output_dir, cls + '_pr.pkl'), 'wb') as f:
                    pickle.dump({'rec': rec, 'prec': prec, 'ap': ap}, f)
        print('Mean AP = {:.4f}'.format(np.mean(aps)))
        print('~~~~~~~~')
        print('Results:')
        for ap in aps:
            print('{:.3f}'.format(ap))
        print('{:.3f}'.format(np.mean(aps)))
        print('~~~~~~~~')


def detection_collate(batch):
    targets = []
    imgs = []
    for _, sample in enumerate(batch):
        for _, tup in enumerate(sample):
            if torch.is_tensor(tup):
                imgs.append(tup)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)

    return (torch.stack(imgs, 0), targets)
