import math
import numpy as np


def rotate(alpha, beta, gamma):
    sin_rx, cos_rx = np.sin(alpha), np.cos(alpha)
    sin_ry, cos_ry = np.sin(beta) , np.cos(beta)
    sin_rz, cos_rz = np.sin(gamma), np.cos(gamma)
    
    roll = np.array([
        [1,      0,       0, 0],
        [0, cos_rx, -sin_rx, 0],
        [0, sin_rx,  cos_rx, 0],
        [0,      0,       0, 1]
    ])
    pitch = np.array([
        [ cos_ry,  0, sin_ry, 0],
        [      0,  1,      0, 0],
        [-sin_ry,  0, cos_ry, 0],
        [      0,  0,      0, 1]
    ])
    yaw = np.array([
        [cos_rz, -sin_rz, 0, 0],
        [sin_rz,  cos_rz, 0, 0],
        [     0,       0, 1, 0],
        [     0,       0, 0, 1]
    ])
    return yaw.dot(pitch).dot(roll)


def translate(tx, ty, tz):
    return np.array([
        [1, 0, 0, tx],
        [0, 1, 0, ty],
        [0, 0, 1, tz],
        [0, 0, 0, 1]
    ])


def perspective_transform(shape,
                          translation=(0, 0, 0),
                          rotation=(0, 0, 0),
                          matrix=None,
                          f=None,
                          ipm=True):
    if len(shape) == 3:
        w, h, _ = shape
    else:
        w, h = shape
    diag = np.sqrt(w ** 2 + h ** 2)
    fov = 120
    if f is None:
        f = (np.sqrt(w ** 2 + h ** 2) / 2) / np.tan(np.deg2rad(fov / 2))

    A = np.array([[1, 0, -w / 2],
                  [0, 1, -h / 2],
                  [0, 0,      0],
                  [0, 0,      1]])

    K = np.array([[f, 0, w / 2, 0],
                  [0, f, h / 2, 0],
                  [0, 0,     1, 0]])
    if matrix is None:
        tx, ty, tz = translation
        rx, ry, rz = rotation

        radx = np.deg2rad(rx)
        rady = np.deg2rad(ry)
        radz = np.deg2rad(rz)

        tz = (f - tz) / 1 ** 2
        T_M = translate(tx, ty, tz)
        R_M = rotate(radx, rady, radz)

        if ipm:
            M = K.dot(T_M).dot(R_M).dot(A)
            M = np.linalg.inv(M)
        else:
            M = K.dot(R_M.dot(T_M)).dot(A)
    else:
        M = K.dot(matrix).dot(A)
        if ipm:
            M = np.linalg.inv(M)
    return M
    

def pnt2cir(center, r, n=360):
    return [(int(center[0] + math.cos(2 * math.pi / n * x) * r), 
             int(center[1] + math.sin(2 * math.pi / n * x) * r)) for x in range(0, n + 1)]


def cir3pnts(pnts):
    x1, y1, x2, y2, x3, y3 = pnts[0][0], pnts[0][1], pnts[1][0], pnts[1][1], pnts[2][0], pnts[2][1]
    c = (x1 - x2)**2 + (y1 - y2)**2
    a = (x2 - x3)**2 + (y2 - y3)**2
    b = (x3 - x1)**2 + (y3 - y1)**2
    s = 2 * (a*b + b*c + c*a) - (a**2 + b**2 + c**2) 
    px = (a * (b + c - a) * x1 + b * (c + a - b) * x2 + c * (a + b - c) * x3) / s
    py = (a * (b + c - a) * y1 + b * (c + a - b) * y2 + c * (a + b - c) * y3) / s 
    ar = a**0.5
    br = b**0.5
    cr = c**0.5 
    r = ar*br*cr / ((ar + br + cr) * (-ar + br + cr) * (ar - br + cr) * (ar + br - cr))**0.5
    return int(r), (int(px), int(py))


def createLineIterator(P1, P2, img):
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
    itbuffer.fill(np.nan)

    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X:
        itbuffer[:, 0] = P1X
        if negY:
            itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
        else:
            itbuffer[:, 1] = np.arange(P1Y+1, P1Y+dYa+1)
    elif P1Y == P2Y:
        itbuffer[:, 1] = P1Y
        if negX:
            itbuffer[:, 0] = np.arange(P1X-1, P1X-dXa-1, -1)
        else:
            itbuffer[:, 0] = np.arange(P1X+1, P1X+dXa+1)
    else:
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32)/dY.astype(np.float32)
            if negY:
                itbuffer[:, 1] = np.arange(P1Y-1, P1Y-dYa-1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y+1, P1Y+dYa+1)
            itbuffer[:, 0] = (slope*(itbuffer[:, 1]-P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32)/dX.astype(np.float32)
            if negX:
                itbuffer[:, 0] = np.arange(P1X-1, P1X-dXa-1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X+1, P1X+dXa+1)
            itbuffer[:, 1] = (slope*(itbuffer[:, 0]-P1X)).astype(np.int) + P1Y

    colX = itbuffer[:, 0]
    colY = itbuffer[:, 1]
    itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

    itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint), itbuffer[:, 0].astype(np.uint)]

    return itbuffer
