import os
import torch
import numpy as np

from PIL import Image
from torchvision import datasets
from torchvision import transforms


class DataGenerator(datasets.ImageFolder):
    def __init__(self, images_list, config, transform=None):
        self.listfile = images_list
        self.images_path = []
        self.labels_path = []
        self.target_size = config.model.input_size
        self.channel_first = config.model.channel_first
        
        if transform is None:
            self.T = transforms.Compose([
                transforms.Resize((self.target_size, self.target_size)), 
                transforms.ToTensor()
            ])
        else:
            self.T = transform
        
        self.prepare_dataset()

    def __len__(self):
        return len(self.images_path)
    
    def __getitem__(self, idx):
        x = self.T(self.image_loader(self.images_path[idx]))
        y = self.label_loader(self.labels_path[idx])
        if not self.channel_first:
            x = x.permute(2, 0, 1)
        return self.images_path[idx], x, y

    def image_loader(self, image_file):
        image = Image.open(image_file)
        image = image.convert('RGB')
        return image
    
    def label_loader(self, label_file):
        label = np.array([[int(y) for y in x.strip('\n').split(",")] for x in open(label_file, "r").readlines()])
        return label
        
    def prepare_dataset(self):
        self.images_path = [x.strip('\n') for x in open(self.listfile, "r").readlines()]
        self.labels_path = [x.replace(".jpg", ".txt").replace(".png", ".txt").replace("data_openfile", "data_openfile_output") for x in self.images_path]