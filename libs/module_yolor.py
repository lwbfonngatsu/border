import sys
sys.path.append("/home/protomate/Desktop/border/networks/yolor")

import cv2
import torch
import numpy as np

from utils.datasets import letterbox
from utils.torch_utils import select_device
from models.experimental import attempt_load
from utils.general import check_img_size, non_max_suppression, scale_coords


class YOLORModule(object):
    def __init__(self, weight="/home/protomate/Desktop/border/weights/YOLOR/yolor-d6.pt", 
                 device='0', imgsize=1280, thr=0.5, iou=0.45, classes=None):
        self.weight = weight
        self.device = device
        self.imgsz = imgsize
        self.conf_thr = thr
        self.iou_thr = iou
        self.classes = classes
        self.load_model()
        
    def load_model(self):
        self.device = select_device(self.device)
        self.model = attempt_load(self.weight, map_location=self.device)
        self.imgsz = check_img_size(self.imgsz, s=self.model.stride.max())
        img = torch.zeros((1, 3, self.imgsz, self.imgsz), device=self.device)
        _ = self.model(img) if self.device.type != 'cpu' else None
        print("Successfully load YOLOR model")

    def predict(self, image):
        img = letterbox(image, new_shape=self.imgsz, auto_size=64)[0]
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img).to(self.device)
        img = img.float()
        img /= 255.0

        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        pred = self.model(img, augment=False)[0]
        pred = non_max_suppression(pred, self.conf_thr, self.iou_thr, classes=self.classes, agnostic=False)
        return pred, img
    
    def draw_prediction(self, image, return_pred=True):
        pred, pred_img = self.predict(image)
        all_preds = []
        for i, det in enumerate(pred):
            gn = torch.tensor(image.shape)[[1, 0, 1, 0]]
            if len(det):
                det[:, :4] = scale_coords(pred_img.shape[2:], det[:, :4], image.shape).round()
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()
                for *xyxy, conf, cls in reversed(det):
                    x1, y1, x2, y2 = [int(a) for a in xyxy]
                    all_preds.append([x1, y1, x2, y2, int(cls)])
                    image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 5)
        if return_pred:
            return image, all_preds
        else:
            return image, None