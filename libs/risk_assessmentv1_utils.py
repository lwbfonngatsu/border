import cv2
import math
import torch
import numpy as np
import matplotlib.pyplot as plt

from utilize.nms import nms
from torch.autograd import Variable
from lib.track.KF import Track


class FrontRearCam:
    def __init__(self, cam, transform, threshold=0.8, nms_threshold=0.45, perspective_pnt=None):
        self.pipeline = Track(max_age=5)
        self.cam = cam
        if perspective_pnt is None:
            if self.cam == 'front':
                self.perspective_pnt = (1750, 1260)
            else:
                self.perspective_pnt = (1850, 1000)
        else:
            self.perspective_pnt = perspective_pnt
        self.num_classes = 21
        self.threshold = threshold
        self.nms_threshold = nms_threshold
        self.transform = transform
        
    def predict(self, frame, net, detector, priors):
        img = Variable(self.transform(frame).unsqueeze(0)).cuda()
        out = net(img)
        w, h = frame.shape[1], frame.shape[0]
        scale = torch.Tensor([w, h, w, h])
        boxes, scores = detector.forward(out, priors)
        boxes = (boxes[0] * scale).cpu().numpy()
        scores = scores[0].cpu().numpy()
        all_boxes = [[] for _ in range(self.num_classes)]

        for j in range(1, self.num_classes):
            if j in [2, 6, 7, 14]:
                inds = np.where(scores[:, j] > self.threshold)[0]
                if len(inds) == 0:
                    all_boxes[j] = np.empty([0, 5], dtype=np.float32)
                    continue
                c_bboxes = boxes[inds]
                c_scores = scores[inds, j]
                c_dets = np.hstack((c_bboxes, c_scores[:, np.newaxis])).astype(np.float32, copy=False)

                keep = nms(c_dets, self.nms_threshold)
                keep = keep[:50]
                c_dets = c_dets[keep, :]
                all_boxes[j] = c_dets

        code = None
        all_risk_code = []
        measured = []
        for label in range(len(all_boxes)):
            if np.any(all_boxes[label]):
                for box in all_boxes[label]:
#                     bbox = [0 if point <= 0 else point for point in box[:4].astype(np.int).tolist()]
                    bbox = [point for point in box[:4].astype(np.int).tolist()]
                    measured.append(bbox)

        track_list = self.pipeline.run(measured)
        obj_pos = []
        for tracker in track_list:
            x, _, y, _, x2, _, y2, _ = tracker.x_state
            x, y, x2, y2 = check_coor(x, y, x2, y2)
            risk, code = risk_assess((x, y, x2, y2), self.perspective_pnt, frame.shape)
            all_risk_code.append(code)
            distance = estimate_distance(1.6, y2 - y)
            if self.cam == 'front':
                center = (int((x2 + x) / 2), int((y2 + y) / 2))
            else:
                center = (int((x2 + x) / 2), max(y, y2))
            obj_pos.append([which_area(frame, center, self.perspective_pnt), distance, risk])
            if risk == 'High':
                color = (0, 0, 255)
            elif risk == 'Medium':
                color = (0, 255, 255)
            else:
                color = (0, 255, 0)
            frame = cv2.rectangle(frame, (x, y), (x2, y2), color, 10)
#             frame = cv2.putText(frame, f"{risk}_{code}: {round(distance[0], 2)} m.", (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)
        return frame, obj_pos
    
    
class SideCam:
    def __init__(self, cam, transform, threshold=0.8, nms_threshold=0.45, perspective_pnt=None):
        self.pipeline = Track(max_age=5)
        self.threshold = threshold
        self.transform = transform
        self.nms_threshold = nms_threshold
        self.cam = cam
        self.num_classes = 21
        if perspective_pnt is None:
            if self.cam == 'left':
                self.perspective_pnt = (75, 650)
            else:
                self.perspective_pnt = (125, 530)
        else:
            self.perspective_pnt = perspective_pnt
        self.risks = ['High', 'Medium', 'Low', 'Low']
    
    def predict(self, frame, net, detector, priors):
        img = Variable(self.transform(frame).unsqueeze(0)).cuda()
        out = net(img)
        w, h = frame.shape[1], frame.shape[0]
        scale = torch.Tensor([w, h, w, h])
        boxes, scores = detector.forward(out, priors)
        boxes = (boxes[0] * scale).cpu().numpy()
        scores = scores[0].cpu().numpy()
        all_boxes = [[] for _ in range(self.num_classes)]

        for j in range(1, self.num_classes):
            if j in [2, 6, 7, 14]:
                inds = np.where(scores[:, j] > self.threshold)[0]
                if len(inds) == 0:
                    all_boxes[j] = np.empty([0, 5], dtype=np.float32)
                    continue
                c_bboxes = boxes[inds]
                c_scores = scores[inds, j]
                c_dets = np.hstack((c_bboxes, c_scores[:, np.newaxis])).astype(np.float32, copy=False)

                keep = nms(c_dets, self.nms_threshold)
                keep = keep[:50]
                c_dets = c_dets[keep, :]
                all_boxes[j] = c_dets

        code = None
        all_risk_code = []
        measured = []
        for label in range(len(all_boxes)):
            if np.any(all_boxes[label]):
                for box in all_boxes[label]:
#                     bbox = [0 if point <= 0 else point for point in box[:4].astype(np.int).tolist()]
                    bbox = [point for point in box[:4].astype(np.int).tolist()]
                    measured.append(bbox)

        track_list = self.pipeline.run(measured)
        obj_pos = []
        for tracker in track_list:
            x, _, y, _, x2, _, y2, _ = tracker.x_state
            x, y, x2, y2 = check_coor(x, y, x2, y2)
            center = (int((x2 + x) / 2), max(y2, y))
            area = side_risk(frame.shape, self.perspective_pnt, center)
            if area >= 0:
                risk = self.risks[area]
            else:
                risk = 'Low'
            distance = estimate_distance(1.5, y2 - y)
#             center = (int((x2 + x) / 2), int((y2 + y) / 2))
            if center[0] < frame.shape[1] * 0.35:
                obj_pos.append([which_lane_side(frame, center, self.perspective_pnt), distance, risk])
            else:
                obj_pos.append([-1, distance, risk])
            if risk == 'High':
                color = (0, 0, 255)
            elif risk == 'Medium':
                color = (0, 255, 255)
            else:
                color = (0, 255, 0)
            frame = cv2.rectangle(frame, (x, y), (x2, y2), color, 2)
#             frame = cv2.putText(frame, f"{risk}_{code}: {round(distance[0], 2)} m.", (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)
        return frame, obj_pos

def draw_uiv2(ui, obj_center_depth, cam, shape=(2160, 2160, 3)):
    ml = ui.shape[1] * 0.19
    l = ui.shape[1] * 0.38
    m = ui.shape[1] * 0.62
    r = ui.shape[1] * 0.81
    mr = ui.shape[1]
    most_left = int(ml / 2)
    left = int((ml + l) / 2)
    mid = int((l + m) / 2)
    right = int((m + r) / 2)
    most_right = int((r + mr) / 2)
    c_area = [most_left, left, mid, right, most_right]
    
    motorcycle = cv2.imread("Python/motorcycle_top.jpg")
    motorcycle = cv2.resize(motorcycle, (int(shape[0] / 6.2), int(shape[1] / 13.6)))
    mw, mh, _ = motorcycle.shape
    agent_center = (int(shape[1]/2), int(shape[0]/2))
    
#     xr = int(round(agent_center[0] + 2160 * math.cos((-90 + 60) * math.pi / 180.0)));
#     yr = int(round(agent_center[1] + 2160 * math.sin((-90 + 60) * math.pi / 180.0)));
#     ui = cv2.line(ui, (agent_center), (xr, yr), (255, 0, 0), 10)
    
#     xl = int(round(agent_center[0] + 2160 * math.cos((-90 - 60) * math.pi / 180.0)));
#     yl = int(round(agent_center[1] + 2160 * math.sin((-90 - 60) * math.pi / 180.0)));
#     ui = cv2.line(ui, (agent_center), (xl, yl), (255, 0, 0), 10)
    
    agent_tl = (agent_center[0] - int(mw/2), agent_center[1] - int(mh/2))
    agent_br = (agent_center[0] + int(mw/2), agent_center[1] + int(mh/2))
    motorcycle = cv2.rotate(motorcycle, cv2.cv2.ROTATE_90_CLOCKWISE)
    ui[agent_tl[1]: agent_tl[1] + mh, agent_tl[0]: agent_tl[0] + mw] = motorcycle

    ui = cv2.circle(ui, agent_center, int(max(motorcycle.shape)*1.25), (0, 0, 255), 10) # red
    ui = cv2.circle(ui, agent_center, int(max(motorcycle.shape)*2.25), (0, 175, 255), 10) # yellow
    ui = cv2.circle(ui, agent_center, int(max(motorcycle.shape)*3.00), (0, 255, 0), 10) # green
    
    for obj in obj_center_depth:
        center, depth, risk = obj
        if center != -1:
            if cam == 'right':
                center = 2 + center
            elif cam == 'left':
                center = 2 - center
            elif cam == 'rear':
                center = 4 - center
            else:
                center = center
                
            if risk == 'High':
                color = (0, 0, 255)
            elif risk == 'Medium':
                color = (0, 255, 255)
            else:
                color = (0, 255, 0)
            cx = c_area[center]
            if cam == 'front':
                cy = agent_center[1] - (depth * 75 * 1.7)
            else:
                cy = agent_center[1] + (depth * 75 * 1.7)
            _x, _y, _x2, _y2 = int(cx - 150/2), int(cy - 300/2), int(cx + 150/2), int(cy + 300/2)
    #         color = color_determine(center, depth)
            ui = cv2.rectangle(ui, (_x, _y), (_x2, _y2), color, -1)
    return ui

def which_area(image, center, perspective_pnt):
    uleft = [(0, perspective_pnt[1]), perspective_pnt, (0, perspective_pnt[1] + 500)]
    left = [(0, perspective_pnt[1] + 500), perspective_pnt, (0, image.shape[0])]
    
    mid = [(0, image.shape[0]), perspective_pnt, (image.shape[1], image.shape[0])]
    
    right = [(image.shape[1], image.shape[0]), perspective_pnt, (image.shape[1], perspective_pnt[1] + 500)]
    uright = [(image.shape[1], perspective_pnt[1] + 500), perspective_pnt, (image.shape[1], perspective_pnt[1])]
    
    contours = [np.array(uleft), np.array(left), np.array(mid), np.array(right), np.array(uright)]
    area = [0, 0, 0, 0, 0]
    for i in range(len(contours)):
        area[i] = int(cv2.pointPolygonTest(contours[i], center, False))
#         image = cv2.drawContours(image, [contours[i]], -1, (0, 0, 0), 5)
#     image = cv2.circle(image, center, 5, (255, 0, 0), 5)
#     plt.imshow(image)
    plt.show()
    if 1 in area:
        return area.index(1)
    else:
        return -1

def which_lane_side(image, center, perspective_pnt):
    xr, yr = int(image.shape[1]), image.shape[0]
    xr2, yr2 = int(image.shape[1]), perspective_pnt[1] + 100
    xp, yp = perspective_pnt
    y_box = center[1]
    x_line1 = int((((xr - xp) / (yr - yp)) * (y_box - yp)) + xp)
    x_line2 = int((((xr2 - xp) / (yr2 - yp)) * (y_box - yp)) + xp)
    if center[0] < x_line2:
        if center[0] < x_line1:
            lane = 0
        else:
            lane = 1
    else:
        lane = 2
    return lane

def define_circle(p1, p2, p3):
    temp = p2[0] * p2[0] + p2[1] * p2[1]
    bc = (p1[0] * p1[0] + p1[1] * p1[1] - temp) / 2
    cd = (temp - p3[0] * p3[0] - p3[1] * p3[1]) / 2
    det = (p1[0] - p2[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p2[1])

    if abs(det) < 1.0e-6:
        return (None, np.inf)

    cx = (bc*(p2[1] - p3[1]) - cd*(p1[1] - p2[1])) / det
    cy = ((p1[0] - p2[0]) * cd - (p2[0] - p3[0]) * bc) / det

    radius = np.sqrt((cx - p1[0])**2 + (cy - p1[1])**2)
    return (int(cx), int(cy)), int(radius)

def side_risk(shape, perspective_pnt, pnt, verbose=False):
    backend = np.zeros(shape, dtype=np.uint8)
    backend = cv2.line(backend, (0, 0), (backend.shape[1], 0), (0, 255, 255), 20)
    backend = cv2.line(backend, (0, 0), (0, backend.shape[0]), (0, 255, 255), 20)
    backend = cv2.line(backend, (backend.shape[1], 0), (backend.shape[1], backend.shape[0]), (0, 255, 255), 20)
    backend = cv2.line(backend, (0, backend.shape[0]), (backend.shape[1], backend.shape[0]), (0, 255, 255), 20)

    center, r = define_circle((0, backend.shape[0]), (int(backend.shape[1]/2), perspective_pnt[1]), (backend.shape[1], perspective_pnt[1]))
    backend = cv2.circle(backend, center, r, (0, 255, 255), 20)
    center, r = define_circle((0, backend.shape[0] - 150), (int(backend.shape[1]/2), perspective_pnt[1] - 150), (backend.shape[1], perspective_pnt[1] - 150))
    backend = cv2.circle(backend, center, r, (0, 255, 255), 20)
    center, r = define_circle((0, backend.shape[0] - 250), (int(backend.shape[1]/2), perspective_pnt[1] - 250), (backend.shape[1], perspective_pnt[1] - 250))
    backend = cv2.circle(backend, center, r, (0, 255, 255), 20)
    graybackend = cv2.cvtColor(backend, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(graybackend, 127, 255, 0)

    colors = [(0, 0, 0), (255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 255)]
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    risks = [0, 0, 0, 0, 0]
    
    for i in range(len(contours)):
        if verbose:
            backend = cv2.drawContours(backend, [contours[i]], -1, (255, 255, 0), 10)
        risks[i] = int(cv2.pointPolygonTest(contours[i], pnt, False))
    del risks[0]
    if 1 in risks:
        if verbose:
            return risks.index(1), backend
        else:
            return risks.index(1)
    else:
        return -1
    
def check_coor(x, y, x2, y2):
    if x > x2: # x1 > x2?
        tmp_x = x
        x = x2
        x2 = tmp_x
    if y > y2: # y1 > y2?
        tmp_y = y
        y = y2
        y2 = tmp_y
    return x, y, x2, y2

def risk_assess(box, perspective_pnt, image_shape, verbose=False):
    risk_code = {
        'LLF': 'Low',
        'LLM': 'Medium',
        'LLN': 'High',
        'LMF': 'Low',
        'LMM': 'Medium',
        'LMN': 'High',
        'LRF': 'Low',
        'LRM': 'Medium',
        'LRN': 'High',
        'MMF': 'Medium',
        'MMM': 'High',
        'MMN': 'High',
        'MRF': 'Low',
        'MRM': 'Medium',
        'MRN': 'High',
        'RRF': 'Low',
        'RRM': 'Medium',
        'RRN': 'High',
    }
    code = ''
    y_box = max(box[1], box[3])
    x, y = perspective_pnt
    x_max, y_max, _ = image_shape
    xl = (x / (y - y_max)) * (y_box - y) + x
    xr = ((x_max - x) / (y_max - y)) * (y_box - y) + x
#     xl = (-1850/1380) * (y_box-780)+1850
#     xr = (1990/1380) * (y_box-780)+1850
    if verbose:
        print(f"Boxl X: {box[0]}\tBoxl Y: {box[1]}\nBoxr X: {box[2]}\tBoxr Y: {box[3]}")
        print(f"xl: {xl}, xr: {xr}, y: {y_box}")
    if box[0] < xl: #left
        code += 'L'
    elif box[0] < xr: #mid
        code += 'M'
    else: #right
        code += 'R'
        
    if box[2] < xl: #left
        code += 'L'
    elif box[2] < xr: #mid
        code += 'M'
    else: #right
        code += 'R'
        
    if y_box < perspective_pnt[1] + 200: #far
        code += 'F'
    elif y_box < perspective_pnt[1] + 500: #meduim
        code += 'M'
    else: #near
        code += 'N'
    if verbose:
        print('Risk code is:', code)
    return risk_code[code], code

def draw_zone(image, perspective_pnt=(1850, 780)):
    image = cv2.circle(image, perspective_pnt, 5, (255, 0, 0), 5)
    red = [(0, 2160), (3840, 2160)]
    green = [(0, perspective_pnt[1]), (3840, perspective_pnt[1])]
    yellow = [(0, perspective_pnt[1] + 500), (3840, perspective_pnt[1] + 500)]
    for point in red:
        image = cv2.line(image, perspective_pnt, point, (255, 0, 0), 2)
    for point in green:
        image = cv2.line(image, perspective_pnt, point, (0, 255, 0), 2)
    for point in yellow:
        image = cv2.line(image, perspective_pnt, point, (255, 255, 0), 2)
    image = cv2.line(image, (0, perspective_pnt[1]+500), (3840, perspective_pnt[1]+500), (255, 0, 0), 2)
    image = cv2.line(image, (0, perspective_pnt[1]+200), (3840, perspective_pnt[1]+200), (255, 255, 0), 2)
    return image

def draw_polygon(code, agent_pos=(750, 1080), shape=(2160, 1500, 3)):
    ui = np.zeros(shape, dtype=np.uint8)
    tl = (agent_pos[0] - 100, agent_pos[1])
    br = (agent_pos[0] + 100, agent_pos[1] + 400)

    line1 = [(0, 0), (300, 500), (481, 800), (650, 1080)]
    line2 = [(400, 0), (483, 360), (567, 720), (650, 1080)]
    line3 = [(1100,0), (1017,360), (933, 720), (850, 1080)]
    line4 = [(1500,0), (1200,500), (1019,800), (850,1080)]
    lines = []
    lines.append(line1)
    lines.append(line2)
    lines.append(line3)
    lines.append(line4)

    polygons = []
    polygons_risk = [
        'L', 'M', 'H',
        'M', 'M', 'H',
        'L', 'M', 'H'
    ]
    polygon_risk_code = {
        'LLF': [0],
        'LLM': [1],
        'LLN': [2],
        'LMF': [0, 3],
        'LMM': [1, 4],
        'LMN': [2, 5],
        'LRF': [0, 3, 6],
        'LRM': [1, 4, 7],
        'LRN': [2, 5, 8],
        'MMF': [3],
        'MMM': [4],
        'MMN': [5],
        'MRF': [3, 6],
        'MRM': [4, 7],
        'MRN': [5, 8],
        'RRF': [6],
        'RRM': [7],
        'RRN': [8],
    }
    for i in range(3):
        for j in range(3):
            polygons.append([lines[i][j], lines[i+1][j], lines[i+1][j+1], lines[i][j+1]])
    ui = cv2.rectangle(ui, tl, br, (255, 255, 255), -1)
    for polygon in polygons:
        ui = cv2.polylines(ui, [np.array(polygon)], True, (255, 255, 255), 5)
    if code is not None:
        for c in code:
            index = polygon_risk_code[c]
            for i in index:
                if polygons_risk[i] == 'L':
                    color = (0, 255, 0)
                if polygons_risk[i] == 'M':
                    color = (255, 255, 0)
                if polygons_risk[i] == 'H':
                    color = (255, 0, 0)
                ui = cv2.fillPoly(ui, [np.array(polygons[i])], color)
    ui = cv2.cvtColor(ui, cv2.COLOR_BGR2RGB)
    return ui

def estimate_distance(w, w_hat):
    ref_w_pixel = 440
    ref_w_real = 1.8
    ref_dis_real = 4.5
    focal_length = (ref_w_pixel * ref_dis_real) / ref_w_real
    return abs((w * focal_length) / w_hat)