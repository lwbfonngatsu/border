import pandas
import random
import datetime

from pandas.core.frame import DataFrame


TEMPLATE = """
Name: %s
Age: %s
Last Visit: %s
Favorite Order:
    %s
    %s
"""

PERSON_INFO = {
    "Feihong": 23,
    "Poon": 24,
    "Lisa": 23
}

TYPES = [
#     'Hot',
    'Iced',
#     'Blended'
]

MENUS = [
#     'Cappuccino',
#     'Mocha',
#     'Latte',
#     'Macchiato',
#     'Americano',
#     'Espresso',
    'Matcha Latte'
]

SWEET_LEVEL = [
    "Sweet 0%",
#     "Sweet 25%",
#     "Sweet 50%",
#     "Sweet 75%",
#     "Normal Sweet",
#     "Sweet 125%",
#     "Sweet 150%"
]

START_DATE = datetime.date(2021, 1, 1)
LIMIT_DATE = datetime.date.today() - datetime.timedelta(days=1)
TIME_BETWEEN_DATE = LIMIT_DATE - START_DATE
DAYS_BETWEEN_DATE = TIME_BETWEEN_DATE.days

DAYS = [
#     "Sunday",
#     "Monday",
#     "Tuesday",
#     "Wednesday",
#     "Thursday",
#     "Friday",
    "Saturday"
]

MEMBERSHIP_TIER = [
#     "Bronze",
#     "Silver",
#     "Gold",
    "Platinum"
]

def random_order(norders):
    line = ""
    orders = 0
    while True:
        if orders == norders:
            break
        order = f"{random.choice(TYPES)} {random.choice(MENUS)} {random.choice(SWEET_LEVEL)}"
        if order not in line:
            orders += 1
            line += order + f":{random.randint(1, 10)}" + ("/" if orders < norders else "")
    return line


def random_lv():
    rand_day = random.randrange(DAYS_BETWEEN_DATE)
    last_visit = START_DATE + datetime.timedelta(days=rand_day)
    return last_visit.strftime("%d/%m/%Y")


def create_customer_details():
    dict = {
        "customer_name": list(PERSON_INFO.keys()),
        "age": list(PERSON_INFO.values()),
        "orders_info": [random_order(1), random_order(1), random_order(1)],
        "last_visit": [random_lv(), random_lv(), random_lv()],
        "favorite_visit_day": [random.choice(DAYS), random.choice(DAYS), random.choice(DAYS)],
        "membership_tier": [random.choice(MEMBERSHIP_TIER), random.choice(MEMBERSHIP_TIER), random.choice(MEMBERSHIP_TIER)]
    }
    df = DataFrame(dict)
    df.to_csv("customer_details.csv")


class Person(object):
    def __init__(self, name, age, all_orders, last_visit, fav_visit_day, membership_tier):
        self.name = name
        self.age = age
        self.all_orders = all_orders
        self.last_visit = last_visit
        self.fav_visit_day = fav_visit_day
        self.membership_tier = membership_tier

    def __call__(self):
        return TEMPLATE % (self.name, str(self.age), self.last_visit, self.get_fav().replace(" Sweet 0%", ""), "Sweet 0%")
    
    def update(self, order):
        if order in self.all_orders:
            self.all_orders[order] += 1
        else:
            self.all_orders[order] = 1

    def get_fav(self):
        return max(self.all_orders.keys(), key=(lambda key: self.all_orders[key]))


class CustomerHandler(object):
    def __init__(self, path="customer_details.csv"):
        self.path = path
        self.data_bank = {}
        self.customers_information = self.info_extract()
        
    def info_extract(self):
        df = pandas.read_csv(self.path)
        for _, row in df.iterrows():
            name = row['customer_name']
            if name != "Unknown":
                age = row['age']
                orders_info = row['orders_info'].split("/")
                beverages = {}
                for beverage in orders_info:
                    menu = beverage.split(":")[0]
                    frequence = beverage.split(":")[1]
                    beverages[menu] = int(frequence)
                last_visit = row['last_visit']
                fav_visit_day = row['favorite_visit_day']
                membership_tier = row['membership_tier']
                self.data_bank[name] = Person(name, age, beverages, last_visit, fav_visit_day, membership_tier)
            else:
                self.data_bank[name] = Person(name, name, name, name, name, name)

    def update_databank(self, person, order=''):
        # Call when person received coffee
        if person in self.data_bank:
            order = random.choice(MENUS)
            self.data_bank[person].update(order)
        else:
            pass
    
    def __call__(self, name):
        if name in self.data_bank:
            return self.data_bank[name]()
        else:
            return f"Do not found the person named {name} in the database."


create_customer_details()

if __name__ == "__main__":
    create_customer_details()
    customer_handler = CustomerHandler()
    customer_handler.update_databank('Poon')
    print(customer_handler('Poon'))
    print(customer_handler('Feihong'))
